/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-06T15:53:04+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-24T09:00:53+01:00
 * @Copyright: The Distance
 */

/* eslint-disable camelcase */

const http = require('http');
const path = require('path');
const R = require('ramda');
const express = require('express');
const ParseServer = require('parse-server').ParseServer;

const noop = () => {};

function createServer({
  cloudCode, // Path to parse cloud functions
  emailAdapter = require('./lib/email'),
  expressOverrides = noop, // Yields the express app before all other routes are passed to the React UI
  customLogger = null,
  liveQuery = undefined, // Parse server liveQuery option
  parseConfig = {}, // Extra config to pass to Parse Server.
  publicPath, // Path to the public directory
  webpackConfig, // The webpack to use to hot reload in development
}) {
  let appPort = process.env.APP_PORT;

  const PhusionPassenger = global.PhusionPassenger;

  if (PhusionPassenger) {
    PhusionPassenger.configure({ autoInstall: false });
    appPort = 'passenger';
  }

  const app = express();

  const nodeEnv = process.env.NODE_ENV;
  const appId = process.env.APP_ID;
  const appName = process.env.APP_NAME || appId;
  const masterKey = process.env.MASTER_KEY;
  const serverURL = process.env.SERVER_URL;
  const databaseURI = process.env.DATABASE_URI;

  /*
   * Initialize Parse Server
   */

  const config = {
    appId,
    appName,
    masterKey,
    serverURL,
    publicServerURL: serverURL,
    databaseURI,
    cloud: cloudCode,
    liveQuery,
  };

  const bucket = process.env.S3_BUCKET;

  if (bucket) {
    config.filesAdapter = {
      module: 'parse-server-s3-adapter',
      options: {
        bucket,
      },
    };
  }

  if (emailAdapter) {
    config.emailAdapter = emailAdapter;
  }

  /*
   * LinkedIn auth
   */

  const useLinkedIn = process.env.USE_LINKEDIN_AUTH;

  if (useLinkedIn) {
    if (!config.auth) config.auth = {};
    config.auth.linkedin = {
      module: require('./lib/linkedin/auth'),
    };
  }

  /*
   * Twitter Settings
   */

  const consumer_key = process.env.TWITTER_CONSUMER_KEY;
  const consumer_secret = process.env.TWITTER_CONSUMER_SECRET;

  if (consumer_key && consumer_secret) {
    if (!config.auth) config.auth = {};
    config.auth.twitter = {
      consumer_key,
      consumer_secret,
    };
  }

  /*
   * Push Settings (Android)
   */

  const senderId = process.env.ANDROID_SENDER_ID;
  const apiKey = process.env.ANDROID_API_KEY;

  if (senderId && apiKey) {
    if (!config.push) config.push = {};
    config.push.android = {
      senderId,
      apiKey,
    };
  }

  /*
   * Push Settings (iOS)
   */

  const strToBool = str => str === 'true';

  const bundleId = process.env.IOS_BUNDLE_ID;
  const passphrase = process.env.IOS_PASSPHRASE;
  const iOSPushProduction = process.env.IOS_PUSH_PRODUCTION;
  const pushProduction =
    typeof iOSPushProduction === 'string'
      ? strToBool(iOSPushProduction)
      : nodeEnv === 'production';
  const pfx = process.env.IOS_PFX;

  if (bundleId && passphrase && pfx) {
    if (!config.push) config.push = {};
    config.push.ios = {
      bundleId,
      passphrase,
      pfx,
      production: pushProduction,
    };
  }

  /*
   * Custom logging
   */

  if (customLogger) {
    config.loggerAdapter =
      typeof customLogger === 'function'
        ? customLogger
        : require('./lib/logger');
  }

  /*
   * Use a dummy push adapter when in development
   */

  if (process.env.USE_DUMMY_PUSH) {
    console.warn(`Using dummy push adapter...`);
    const ConsolePushAdapter = require('./lib/consolePushAdapter');
    if (!config.push) config.push = {};
    config.push.adapter = ConsolePushAdapter;
  }

  const api = new ParseServer(R.merge(config, parseConfig));

  app.use('/parse', api);

  /*
   * Handle static file serving
   */

  app.use(express.static(publicPath));

  /*
   * Developer convenience
   */

  if (nodeEnv === 'development') {
    /*
       * Parse Dashboard
       */

    const ParseDashboard = require('parse-dashboard');
    const dashboard = new ParseDashboard({
      apps: [
        {
          serverURL,
          appId,
          masterKey,
          appName,
        },
      ],
    });

    app.use('/dashboard', dashboard);

    /*
       * Development email adapter
       */

    const MailDev = require('maildev');
    const proxyMiddleware = require('http-proxy-middleware');

    const mailDev = new MailDev({
      basePathname: '/maildev',
      incomingUser: 'mail',
      incomingPass: 'pass',
    });

    mailDev.listen();

    const proxy = proxyMiddleware('/maildev', {
      target: `http://localhost:1080`,
      ws: true,
    });

    app.use(proxy);

    /*
     * Hot reloading
     */

    if (webpackConfig) {
      const webpack = require('webpack');
      const compiler = webpack(webpackConfig);

      app.use(
        require('webpack-dev-middleware')(compiler, {
          noInfo: true,
          publicPath: webpackConfig.output.publicPath,
          watchOptions: {
            poll: 1000,
          },
        })
      );

      app.use(require('webpack-hot-middleware')(compiler));
    }
  }

  /*
   * Pass the express app out to the consumer before
   * implementing the wildcard routing
   */

  expressOverrides(app);

  /*
   * Pass through any other routes to the React UI.
   */

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(publicPath, 'index.html'));
  });

  /*
   * Expose.
   */

  const httpServer = http.createServer(app);

  httpServer.listen(appPort, () => {
    console.warn(`core-server | ${appId} | now listening...`);
  });

  /*
   * Enable the Live Query real-time server
   */

  if (liveQuery) {
    ParseServer.createLiveQueryServer(httpServer);
  }
}

module.exports = createServer;
