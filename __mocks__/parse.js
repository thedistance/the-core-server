/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-26T12:36:24+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-12-07T09:19:15+00:00
 * @Copyright: The Distance
 */

const Parse = require.requireActual('parse');

const send = jest.fn().mockImplementation((...args) => Promise.resolve(...args));

Parse.Push = {
  send,
};

module.exports = Parse;
