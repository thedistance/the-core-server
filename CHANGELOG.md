# 1.11.0
* Update parse-server-s3-adapter to support byte-range request (important for
  streaming content)

# 1.10.7

* Update OTA release error message per client request.

# 1.10.6

* Improve error message for OTA releases when not on iOS / Android.

# 1.10.5

* Better error handling for push scheduler.

# 1.10.4

* Allow a pending notification to target multiple installations.
* Bump the-core-utils.

# 1.10.3

* Update ota releases to support multiple app name, bundle id pairs.

# 1.10.2

* Resolve an issue where the manifest was not being saved in the correct format.

# 1.10.1

* Extract ota release validation/manifest creation from the before save handler.

# 1.10.0

* Add custom logger adapter.

# 1.9.3

* Allow login permissions to work with a session token instead of a username.

# 1.9.2

* Make webpack configuration opt-in.

# 1.9.1

* Add permission merging to login permissions.

# 1.9.0

* Add API for findAllById, which returns parse objects along with the
  user's permissions.

# 1.8.1

* Allow parse config to be extended with further configuration options in
  consuming code.

# 1.8.0

* Add APIs for findAll, findOne and findRelation, which return parse objects
  along with the user's permissions.

# 1.7.0

* Add loginPermissions feature.

# 1.6.3

* Fix server permissions from the last patch.

# 1.6.2

* Implement a more sensible limit for push notification scheduling.

# 1.6.1

* Simplify authentication logic.

# 1.6.0

* Add over the air download feature.
* Consolidate request success/error handling.

# 1.5.2

* Enable customisation of queries from time sensitive push cloud code.
* Minor tweaks to existing code.

# 1.5.1

* Enable the email adapter to be set in the server options.

# 1.5.0

* Add a handler for checking that an admin user can login.

# 1.4.4

* Fix a bug where the server would crash if expressOverrides was not set.

# 1.4.3

* Adding missing dependencies to package.json.

# 1.4.2

* Resolves an issue with an unsupported LinkedIn authentication endpoint; if
  401 is returned then we try a different authentication method.

# 1.4.1

* Further extraction needed of sendBetweenOffsets in order to send custom
  push notifications.

# 1.4.0

* Expose an unwrapped variant of sendBetweenOffsets for other cloud code
  to consume.

# 1.3.2

* Cast env flag set in the last patch to be a boolean.

# 1.3.1

* Add a separate env flag to determine if push messaging is in sandbox mode or
  production mode.

# 1.3.0

* Deprecated list function for push messages.
* Add new listMessages function which may be filtered/sorted/paginated.

# 1.2.0

* Implement timezone sensitive notifications.

# 1.1.0

* Implement custom LinkedIn authentication.

# 1.0.5

* Resolve an issue where production email configuration was incorrectly set up.

# 1.0.4

* Resolve an issue where an incorrect payload for push messages was being sent
  to the server.

# 1.0.3

* Add missing parse-dashboard dependency to the package.json.

# 1.0.2

* Move runtime development dependencies to the regular dependencies field.

# 1.0.1

* Add a prepare script so that development dependencies will be installed
  from npm.

# 1.0.0

* Initial implementation.
