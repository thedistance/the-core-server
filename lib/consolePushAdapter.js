/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-19T17:50:35+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:25:57+01:00
 * @Copyright: The Distance
 */

class ConsolePushAdapter {
  getValidPushTypes() {
    return [];
  }

  send(data, installations) {
    console.warn(`Sending push =>`, JSON.stringify(data));
    return Promise.all(installations.map(() => ({ transmitted: true })));
  }
}

module.exports = ConsolePushAdapter;
