/*
 * @Author: benbriggs
 * @Date:   2018-05-23T13:47:30+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T13:53:39+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const responseSuccess = require('the-core-utils/src/responseSuccess');
const mergeDataWithRoles = require('./mergeDataWithRoles');
const mapPermissionsToUserRoles = require('./mapPermissionsToUserRoles');
const pageCount = require('./pageCount');

const formatQueryResults = (pageSize, request, response) =>
  R.converge(R.compose(responseSuccess(response), R.unapply(R.mergeAll)), [
    R.compose(pageCount(pageSize), R.prop(0)),
    R.compose(
      R.objOf('data'),
      R.apply((permissions, data, roles) =>
        R.map(mergeDataWithRoles(permissions, request.user, roles), data)
      ),
      R.props([3, 1, 2])
    ),
    R.compose(
      R.objOf('permissions'),
      R.map(R.T),
      mapPermissionsToUserRoles,
      R.props([3, 2])
    ),
  ]);

module.exports = formatQueryResults;
