/*
 * @Author: benbriggs
 * @Date:   2018-05-23T09:59:26+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T12:36:28+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const takeNArgs = require('the-core-utils/src/takeNArgs');
const mapPermissionsToUserRoles = require('./mapPermissionsToUserRoles');
const canWriteObject = require('./canWriteObject');

const getWritePermissions = R.curry(
  R.compose(
    R.filter(Boolean),
    R.converge(R.map, [
      R.compose(R.always, R.apply(canWriteObject), R.props([1, 2, 3])),
      mapPermissionsToUserRoles,
    ]),
    takeNArgs(4)
  )
);

module.exports = getWritePermissions;
