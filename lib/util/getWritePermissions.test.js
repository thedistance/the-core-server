/*
 * @Author: benbriggs
 * @Date:   2018-05-23T09:25:44+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T11:43:40+01:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const R = require('ramda');
const getWritePermissions = require('./getWritePermissions');

const Bar = Parse.Object.extend('Bar');

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should return an empty object when class level permissions block user role', () =>
  Promise.all([
    new Parse.Role('Subscriber', new Parse.ACL()).save(),
    new Parse.User().save(),
    new Bar().save(),
  ])
    .then(([role, user, object]) => {
      role.add('users', user);
      return role.save().then(
        R.always([
          {
            find: {
              'role:Super User': true,
            },
            get: {
              'role:Super User': true,
            },
            create: {
              'role:Super User': true,
            },
            update: {
              'role:Super User': true,
            },
            delete: {
              'role:Super User': true,
            },
          },
          [role],
          user,
          object,
        ])
      );
    })
    .then(R.apply(getWritePermissions))
    .then(permission => expect(permission).toEqual({})));

test('should return the appropriate permissions when class level permissions allow user role', () =>
  Promise.all([
    new Parse.Role('Super User', new Parse.ACL()).save(),
    new Parse.User().save(),
    new Bar().save(),
  ])
    .then(([role, user, object]) => {
      role.add('users', user);
      return role.save().then(
        R.always([
          {
            find: {
              'role:Super User': true,
            },
            get: {
              'role:Super User': true,
            },
            create: {
              'role:Super User': true,
            },
            update: {
              'role:Super User': true,
            },
            delete: {
              'role:Super User': true,
            },
          },
          [role],
          user,
          object,
        ])
      );
    })
    .then(R.apply(getWritePermissions))
    .then(permission =>
      expect(permission).toEqual({
        create: true,
        delete: true,
        update: true,
      })
    ));

test('should also apply acl permissions from the object (master key only)', () =>
  Promise.all([
    new Parse.Role('Super User', new Parse.ACL()).save(),
    new Parse.User().save(),
    new Bar().save().then(bar => {
      bar.setACL(new Parse.ACL());
      return bar.save();
    }),
  ])
    .then(([role, user, object]) => {
      role.add('users', user);
      return role.save().then(
        R.always([
          {
            find: {
              'role:Super User': true,
            },
            get: {
              'role:Super User': true,
            },
            create: {
              'role:Super User': true,
            },
            update: {
              'role:Super User': true,
            },
            delete: {
              'role:Super User': true,
            },
          },
          [role],
          user,
          object,
        ])
      );
    })
    .then(R.apply(getWritePermissions))
    .then(permission => expect(permission).toEqual({})));

test('should also apply acl permissions from the object (super user only)', () =>
  Promise.all([
    new Parse.Role('Super User', new Parse.ACL()).save(),
    new Parse.User().save(),
    new Bar().save().then(bar => {
      const acl = new Parse.ACL();
      acl.setRoleWriteAccess('Super User', true);
      bar.setACL(acl);
      return bar.save();
    }),
  ])
    .then(([role, user, object]) => {
      role.add('users', user);
      return role.save().then(
        R.always([
          {
            find: {
              'role:Super User': true,
            },
            get: {
              'role:Super User': true,
            },
            create: {
              'role:Super User': true,
            },
            update: {
              'role:Super User': true,
            },
            delete: {
              'role:Super User': true,
            },
          },
          [role],
          user,
          object,
        ])
      );
    })
    .then(R.apply(getWritePermissions))
    .then(permission =>
      expect(permission).toEqual({
        create: true,
        delete: true,
        update: true,
      })
    ));
