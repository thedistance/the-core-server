/*
 * @Author: benbriggs
 * @Date:   2018-05-23T13:29:22+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T13:53:53+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');

const pageCount = R.curry((pageSize, totalCount) =>
  R.compose(R.objOf('pages'), Math.ceil, R.divide(R.__, pageSize))(totalCount)
);

module.exports = pageCount;
