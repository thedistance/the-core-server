/*
 * @Author: benbriggs
 * @Date:   2018-05-23T12:25:33+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T13:53:48+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const getProperty = require('the-core-utils/src/getProperty');

const mapPermissionsToUserRoles = R.converge(
  (roles, permissions) =>
    R.filter(x => R.any(role => R.has(role, x), roles), permissions),
  [
    R.compose(
      R.prepend('*'),
      R.map(R.compose(R.concat('role:'), getProperty('name'))),
      R.prop(1)
    ),
    R.compose(R.pick(['create', 'update', 'delete']), R.prop(0)),
  ]
);

module.exports = mapPermissionsToUserRoles;
