/*
 * @Author: benbriggs
 * @Date:   2018-05-21T16:02:43+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T11:36:52+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');

const canWriteObject = R.curry((roles, user, object) =>
  R.compose(
    R.anyPass([
      R.isNil,
      R.invoker(0, 'getPublicWriteAccess'),
      acl => acl.getWriteAccess(user),
      acl => R.any(role => acl.getRoleWriteAccess(role), roles),
    ]),
    R.invoker(0, 'getACL')
  )(object)
);

module.exports = canWriteObject;
