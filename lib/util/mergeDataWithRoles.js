/*
 * @Author: benbriggs
 * @Date:   2018-05-22T09:50:16+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T12:17:03+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const getWritePermissions = require('./getWritePermissions');

const mergeDataWithRoles = R.curry((permissions, user, roles, data) =>
  R.converge(R.merge, [
    R.objOf('object'),
    R.compose(
      R.objOf('permissions'),
      getWritePermissions(permissions, roles, user)
    ),
  ])(data)
);

module.exports = mergeDataWithRoles;
