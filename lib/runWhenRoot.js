/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-26T10:07:04+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:26:04+01:00
 * @Copyright: The Distance
 */

function runWhenRoot(request, response) {
  const { user } = request;
  if (!user) {
    response.error('Unauthorised');
    return;
  }

  return new Parse.Query(Parse.Role)
    .equalTo('code', 'root')
    .equalTo('users', user)
    .first()
    .then(isRoot => {
      if (!isRoot) {
        response.error('Unauthorised');
      }
    });
}

module.exports = runWhenRoot;
