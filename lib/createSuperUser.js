/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-10T11:47:37+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:26:00+01:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');

const createSuperUser = () =>
  Promise.all([
    new Parse.Role('Super User', new Parse.ACL())
      .save()
      .then(role => role.save({ code: 'root' })),
    new Parse.User().save(),
  ]).then(([role, user]) => {
    role.add('users', user);
    return role.save().then(() => user);
  });

module.exports = createSuperUser;
