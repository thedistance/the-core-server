/*
 * @Author: benbriggs
 * @Date:   2018-04-24T08:40:23+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-10-31T11:31:30+00:00
 * @Copyright: The Distance
 */

const responseSuccess = require('the-core-utils/src/responseSuccess');
const validateRelease = require('./validateRelease');

const beforeSave = (request, response) =>
  validateRelease(request.object, response).then(responseSuccess(response));

module.exports = beforeSave;
