/*
 * @Author: benbriggs
 * @Date:   2018-04-24T14:17:00+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-10-31T13:25:44+00:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const base64 = require('base-64');
const createManifestFile = require('./createManifestFile');

const createFile = R.flip(R.construct(Parse.File));

// createManifest :: Parse.Object -> Parse.File
const createManifest = R.compose(
  R.partialRight(createFile, ['manifest.plist', 'application/x-plist']),
  R.objOf('base64'),
  base64.encode,
  createManifestFile
);

module.exports = createManifest;
