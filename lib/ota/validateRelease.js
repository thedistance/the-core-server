/**
 * @Author: benbriggs
 * @Date:   2018-10-31T11:27:01+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-10-31T12:02:09+00:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const responseError = require('the-core-utils/src/responseError');
const setProperties = require('the-core-utils/src/setProperties');
const getProperty = require('the-core-utils/src/getProperty');
const createManifest = require('./createManifest');

const promiseOf = x => Promise.resolve(x);

const getPropIsNil = prop => R.compose(R.isNil, getProperty(prop));

const isNew = R.invoker(0, 'isNew');
const setPropertiesOf = R.flip(setProperties);

const validateRelease = (object, response) => {
  if (getPropIsNil('release')(object)) {
    return responseError(response, 'Missing release file.');
  }
  if (getPropIsNil('version')(object)) {
    return responseError(response, 'Missing version number.');
  }
  if (
    R.allPass([isNew, R.compose(R.equals(1), getProperty('platform'))])(object)
  ) {
    return Parse.Promise.resolve(object)
      .then(
        R.composeP(
          setPropertiesOf(object),
          R.objOf('manifest'),
          R.invoker(0, 'save'),
          createManifest,
          promiseOf
        )
      )
      .catch(responseError(response));
  }
  return Parse.Promise.resolve(object);
};

module.exports = validateRelease;
