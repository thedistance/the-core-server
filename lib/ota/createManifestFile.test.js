/**
 * @Author: benbriggs
 * @Date:   2018-10-31T13:26:00+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-10-31T13:32:19+00:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const createManifestFile = require('./createManifestFile');

const OtaRelease = Parse.Object.extend('OtaRelease');

process.env.IOS_APP_NAME = 'CoreServer';
process.env.IOS_BUNDLE_ID = 'uk.co.thedistance.CoreServer';

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should create a manifest with default app name and bundle id', async () =>
  expect(
    createManifestFile(
      await new OtaRelease({
        version: 'v1.0.0',
        release: {
          __type: 'File',
          _name: 'x',
          _url: 'x.ipa',
          url: () => 'x.ipa',
        },
      }).save()
    )
  ).toMatchSnapshot());

test('should create a manifest with custom app name and bundle id', async () =>
  expect(
    createManifestFile(
      await new OtaRelease({
        appName: 'The Core 2',
        bundleId: 'uk.co.thedistance.CoreServer2',
        version: 'v2.0.0',
        release: {
          __type: 'File',
          _name: 'x',
          _url: 'x.ipa',
          url: () => 'x.ipa',
        },
      }).save()
    )
  ).toMatchSnapshot());
