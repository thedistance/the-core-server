/*
 * @Author: benbriggs
 * @Date:   2018-04-24T09:08:17+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:24:05+01:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const getProperty = require('the-core-utils/src/getProperty');
const beforeSave = require('./releaseBeforeSave');

const OtaRelease = Parse.Object.extend('OtaRelease');

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should invalidate the release if the release is unattached', () => {
  ParseMockDB.registerHook('OtaRelease', 'beforeSave', beforeSave);

  return expect(new OtaRelease().save()).rejects.toMatchSnapshot();
});

test('should invalidate the release if the version is undefined', () => {
  ParseMockDB.registerHook('OtaRelease', 'beforeSave', beforeSave);

  return expect(
    new OtaRelease({
      release: {
        __type: 'File',
        _name: 'x',
        _url: 'x.ipa',
        url: () => 'x.ipa',
      },
    }).save()
  ).rejects.toMatchSnapshot();
});

test('should create the application manifest for iOS devices', () =>
  Promise.resolve(
    new OtaRelease({
      release: {
        __type: 'File',
        _name: 'x',
        _url: 'x.ipa',
        url: () => 'x.ipa',
      },
      version: '1.0.0',
      platform: 1,
    })
  )
    .then(object => beforeSave({ object }))
    .then(getProperty('manifest'))
    .then(manifest => expect(manifest).toBeDefined()));

test('should not create the application manifest for Android devices', () =>
  Promise.resolve(
    new OtaRelease({
      release: {
        __type: 'File',
        _name: 'x',
        _url: 'x.apk',
        url: () => 'x.apk',
      },
      version: '1.0.0',
      platform: 2,
    })
  )
    .then(object => beforeSave({ object }))
    .then(getProperty('manifest'))
    .then(manifest => expect(manifest).toBeUndefined()));
