/*
 * @Author: benbriggs
 * @Date:   2018-04-23T14:40:11+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:24:11+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');

const targetPlatform = R.compose(
  R.defaultTo(null),
  R.cond([
    [R.contains('Android'), R.always(2)],
    [R.anyPass([R.contains('iPhone'), R.contains('iPad')]), R.always(1)],
  ]),
  R.path(['headers', 'user-agent'])
);

module.exports = targetPlatform;
