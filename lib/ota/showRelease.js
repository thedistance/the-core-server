/*
 * @Author: benbriggs
 * @Date:   2018-04-23T14:32:34+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:24:08+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const getSessionToken = require('the-core-utils/src/getSessionToken');
const isDefined = require('the-core-utils/src/isDefined');
const responseError = require('the-core-utils/src/responseError');
const responseSuccess = require('the-core-utils/src/responseSuccess');
const targetPlatform = require('./targetPlatform');

// This error is shown when no downloads are found for a user logged in on iOS/Android
const noDownloads = R.always('No downloads were found for your platform.');
// This error is shown when a user logs in on an unsupported platform (usually desktop web)
const unsupportedPlatform = R.always(
  "You don't have the correct permissions to access the CMS. To download the latest version of the app, please login on your mobile device."
);

const show = (request, response) => {
  const user = R.prop('user', request);
  const error = responseError(response);
  if (!user) {
    return error('Unauthorised');
  }

  return Promise.resolve(getSessionToken(user)).then(token =>
    R.ifElse(
      isDefined,
      R.compose(
        R.ifElse(
          isDefined,
          R.composeP(
            R.ifElse(
              isDefined,
              responseSuccess(response),
              R.compose(error, noDownloads)
            ),
            platform =>
              new Parse.Query('OtaRelease')
                .equalTo('platform', platform)
                .descending('createdAt')
                .first(token)
          ),
          R.compose(error, unsupportedPlatform)
        ),
        targetPlatform,
        R.always(request)
      ),
      R.compose(error, R.always('Unauthorised'))
    )(token)
  );
};

module.exports = show;
