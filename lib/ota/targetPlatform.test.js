/*
 * @Author: benbriggs
 * @Date:   2018-04-23T14:43:06+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:24:15+01:00
 * @Copyright: The Distance
 */

const targetPlatform = require('./targetPlatform');

test('should return 1 for iPad user agent strings', () =>
  expect(
    targetPlatform({
      headers: {
        'user-agent':
          'Mozilla/5.0 (iPad; CPU OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) CriOS/30.0.1599.12 Mobile/11A465 Safari/8536.25 (3B92C18B-D9DE-4CB7-A02A-22FD2AF17C8F)',
      },
    })
  ).toEqual(1));

test('should return 1 for iPhone user agent strings', () =>
  expect(
    targetPlatform({
      headers: {
        'user-agent':
          'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53',
      },
    })
  ).toEqual(1));

test('should return 2 for Android user agent strings', () =>
  expect(
    targetPlatform({
      headers: {
        'user-agent':
          'Mozilla/5.0 (Linux; U; Android 2.2.1; en-gb; HTC_DesireZ_A7272 Build/FRG83D) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
      },
    })
  ).toEqual(2));

test('should return null in all other cases', () =>
  expect(
    targetPlatform({
      headers: {
        'user-agent':
          'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36',
      },
    })
  ).toEqual(null));
