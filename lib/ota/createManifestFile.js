/**
 * @Author: benbriggs
 * @Date:   2018-10-31T13:24:12+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-10-31T13:59:41+00:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const getProperties = require('the-core-utils/src/getProperties');
const renameKeys = require('the-core-utils/src/renameKeys');

// appManifest :: Object -> String
const appManifest = ({
  version,
  url,
  bundleId = process.env.IOS_BUNDLE_ID,
  appName = process.env.IOS_APP_NAME,
}) => `<?xml version="1.0" encoding="UTF-8"?>
 <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
 <plist version="1.0">
 <dict>
   <key>items</key>
   <array>
       <dict>
           <key>assets</key>
           <array>
               <dict>
                   <key>kind</key>
                   <string>software-package</string>
                   <key>url</key>
                   <string>${url}</string>
               </dict>
           </array>
           <key>metadata</key>
           <dict>
               <key>bundle-identifier</key>
               <string>${bundleId}</string>
               <key>bundle-version</key>
               <string>${version}</string>
               <key>kind</key>
               <string>software</string>
               <key>title</key>
               <string>${appName}</string>
           </dict>
       </dict>
   </array>
 </dict>
 </plist>
 `;

// createManifestFile :: Parse.Object -> String
const createManifestFile = R.compose(
  appManifest,
  renameKeys({ release: 'url' }),
  R.evolve({
    release: R.invoker(0, 'url'),
  }),
  getProperties(['version', 'release', 'bundleId', 'appName'])
);

module.exports = createManifestFile;
