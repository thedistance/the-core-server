/*
 * @Author: benbriggs
 * @Date:   2018-04-24T14:18:23+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:23:38+01:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const createManifest = require('./createManifest');

const OtaRelease = Parse.Object.extend('OtaRelease');

test('should create a manifest file from a parse object', () =>
  expect(
    createManifest(
      new OtaRelease({
        release: {
          __type: 'File',
          _name: 'x',
          _url: 'x.ipa',
          url: () => 'x.ipa',
        },
        version: '1.0.0',
        platform: 1,
      })
    )
  ).toMatchSnapshot());
