/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-16T12:32:52+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:24:21+01:00
 * @Copyright: The Distance
 */

function getBounds(date, time) {
  const [hour, minute] = time.split(':');

  return date.clone().set({ hour, minute });
}

module.exports = getBounds;
