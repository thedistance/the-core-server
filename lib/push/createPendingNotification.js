/**
 * @Author: benbriggs
 * @Date:   2018-11-12T13:44:08+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-11-12T13:44:46+00:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const getProperty = require('the-core-utils/src/getProperty');
const getFirstOccurrence = require('./getFirstOccurrence');

const PendingNotification = Parse.Object.extend('PendingNotification');

// createPendingNotification :: Parse.Object -> Parse.Object -> Parse.Object
const createPendingNotification = R.curry((installation, notification) => {
  const trigger = getProperty('trigger', notification);
  const timeZone = getProperty('timeZone', installation);
  return new PendingNotification({
    installation,
    notification,
    sendAfter: getFirstOccurrence(new Date(), trigger, timeZone).toDate(),
  }).save(null, { useMasterKey: true });
});

module.exports = createPendingNotification;
