/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-19T16:24:51+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-11-12T10:39:59+00:00
 * @Copyright: The Distance
 */

const responseError = require('the-core-utils/src/responseError');
const responseSuccess = require('the-core-utils/src/responseSuccess');
const runWhenRoot = require('../runWhenRoot');
const sendMessage = require('./sendMessage');

function send(request, response) {
  return runWhenRoot(request, response)
    .then(() => {
      const { message: alert } = request.params;
      if (!alert) {
        responseError(response, 'Your message cannot be blank.');
        return;
      }
      return sendMessage(new Parse.Query(Parse.Installation), {
        alert,
      });
    })
    .then(responseSuccess(response))
    .catch(responseError(response));
}

module.exports = send;
