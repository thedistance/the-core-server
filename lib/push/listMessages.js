/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-10T12:27:27+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:25:06+01:00
 * @Copyright: The Distance
 */

const responseError = require('the-core-utils/src/responseError');
const responseSuccess = require('the-core-utils/src/responseSuccess');
const runWhenRoot = require('../runWhenRoot');

function listMessages(request, response) {
  return runWhenRoot(request, response).then(() => {
    const { pageSize = 10, page = 0, sorted, filtered } = request.params || {};

    const pushStatus = new Parse.Query('_PushStatus')
      .descending('createdAt')
      .limit(pageSize)
      .skip(pageSize * page);

    if (filtered && filtered.length) {
      filtered.forEach(({ id, value }) =>
        pushStatus.matches(id, new RegExp(escape(value), 'i'))
      );
    }

    if (sorted && sorted.length) {
      sorted.forEach(
        ({ id, desc }) =>
          desc ? pushStatus.descending(id) : pushStatus.ascending(id)
      );
    }

    return Promise.all([
      new Parse.Query('_PushStatus').count({ useMasterKey: true }),
      pushStatus.find({ useMasterKey: true }),
    ])
      .then(([count, data]) => ({
        data,
        pages: Math.ceil(count / pageSize),
      }))
      .then(responseSuccess(response))
      .catch(responseError(response));
  });
}

module.exports = listMessages;
