/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-10T12:28:06+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:25:10+01:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const listMessages = require('./listMessages');
const createSuperUser = require('./../createSuperUser');

const PushStatus = Parse.Object.extend('_PushStatus');

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should list push messages', () =>
  Promise.all(
    Array.from(new Array(15)).map((element, index) =>
      new PushStatus({ index }).save()
    )
  )
    .then(() => createSuperUser())
    .then(user => listMessages({ user }))
    .then(statuses => expect(statuses.data).toHaveLength(10))
    .then());

test('should paginate the results', () =>
  Promise.all(
    Array.from(new Array(15)).map((element, index) =>
      new PushStatus({ index }).save()
    )
  )
    .then(() => createSuperUser())
    .then(user => listMessages({ user, params: { pageSize: 5 } }))
    .then(statuses => expect(statuses.pages).toEqual(3)));

test('should filter the results', () =>
  Promise.all([
    ...Array.from(new Array(5)).map((element, index) =>
      new PushStatus({ index, status: 'failed' }).save()
    ),
    ...Array.from(new Array(5)).map((element, index) =>
      new PushStatus({ index, status: 'successful' }).save()
    ),
  ])
    .then(() => createSuperUser())
    .then(user =>
      listMessages({
        user,
        params: {
          pageSize: 10,
          filtered: [{ id: 'status', value: 'successful' }],
        },
      })
    )
    .then(statuses => expect(statuses.data).toHaveLength(5)));

test('should order the results by index', () =>
  Promise.all(
    Array.from(new Array(10)).map((element, index) =>
      new PushStatus({ index }).save()
    )
  )
    .then(() => createSuperUser())
    .then(user =>
      listMessages({ user, params: { sorted: [{ id: 'index', desc: false }] } })
    )
    .then(statuses => expect(statuses.data[0].get('index')).toEqual(0)));
