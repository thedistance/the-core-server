/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-16T12:27:30+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:25:33+01:00
 * @Copyright: The Distance
 */

const moment = require('moment-timezone');
const getBounds = require('./getBounds');

const PendingNotification = Parse.Object.extend('PendingNotification');

function createMasterKeyPendingNotification(params) {
  const pendingNotification = new PendingNotification(params);
  pendingNotification.setACL(new Parse.ACL());
  return pendingNotification.save(null, { useMasterKey: true });
}

function sendTimeSensitivePush(
  notification,
  {
    date,
    from,
    until,
    query = new Parse.Query(Parse.Installation).limit(10000),
  }
) {
  return query.find({ useMasterKey: true }).then(installations =>
    installations.reduce(
      (promise, installation) =>
        promise.then(() => {
          const now = moment.tz(date, installation.get('timeZone'));

          const fromDate = getBounds(now, from);
          const untilDate = getBounds(now, until);

          if (now.isBefore(fromDate)) {
            return createMasterKeyPendingNotification({
              installation,
              notification,
              sendAfter: fromDate.toDate(),
            });
          }

          if (now.isAfter(untilDate)) {
            return createMasterKeyPendingNotification({
              installation,
              notification,
              sendAfter: fromDate.add(1, 'day').toDate(),
            });
          }

          return createMasterKeyPendingNotification({
            installation,
            notification,
            sendAfter: now.toDate(),
          });
        }),
      Promise.resolve()
    )
  );
}

module.exports = sendTimeSensitivePush;
