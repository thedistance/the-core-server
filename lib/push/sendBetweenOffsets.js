/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-09T12:23:16+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:25:23+01:00
 * @Copyright: The Distance
 */

const responseError = require('the-core-utils/src/responseError');
const responseSuccess = require('the-core-utils/src/responseSuccess');
const runWhenRoot = require('./../runWhenRoot');
const unwrappedSend = require('./unwrappedSendBetweenOffsets');

function sendBetweenOffsets(request, response) {
  return runWhenRoot(request, response).then(() =>
    unwrappedSend(request.params)
      .then(() => true)
      .then(responseSuccess(response))
      .catch(responseError(response))
  );
}

module.exports = sendBetweenOffsets;
