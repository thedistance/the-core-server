/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-27T15:26:34+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:24:24+01:00
 * @Copyright: The Distance
 */

const moment = require('moment-timezone');
const getNextOccurrence = require('./getNextOccurrence');

function getFirstOccurrence(date, trigger, timezone) {
  if (!trigger.includes('installation')) {
    return getNextOccurrence(date, trigger, timezone);
  }

  const [, time] = trigger.split('/');
  const addition = {};

  if (time.includes(':')) {
    const [hour, minute] = time.split(':');
    addition.hour = hour;
    addition.minute = minute;
  } else {
    const [num, unit] = time.split(' ');
    addition[unit] = num;
  }

  return moment.tz(date, timezone).add(addition);
}

module.exports = getFirstOccurrence;
