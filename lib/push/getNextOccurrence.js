/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-27T09:18:43+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:24:32+01:00
 * @Copyright: The Distance
 */

const moment = require('moment-timezone');

function getNextOccurrence(date, repeat, timezone) {
  const [interval, time] = repeat.split('/');
  const [hour, minute] = time.split(':');

  const now = moment.tz(date, timezone).utc();
  const next = moment
    .tz(date, timezone)
    .set({
      hour,
      minute,
      second: 0,
      millisecond: 0,
    })
    .utc();

  if (interval === 'daily') {
    next.add(1, 'day');
  } else {
    const targetDay = next.clone().isoWeekday(interval);
    const targetDayIso = targetDay.isoWeekday();
    const nextIso = next.isoWeekday();
    if (nextIso === targetDayIso && now.isBefore(next)) {
      return next;
    } else if (nextIso < targetDayIso) {
      return targetDay;
    }
    next.add(1, 'week').isoWeekday(interval);
  }

  return next;
}

module.exports = getNextOccurrence;
