/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-26T10:50:21+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-11-12T12:44:25+00:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const moment = require('moment');
const createSchedule = require('./schedule');

const Installation = Parse.Object.extend('_Installation');
const PendingNotification = Parse.Object.extend('PendingNotification');
const Notification = Parse.Object.extend('Notification');

const schedule = createSchedule();

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

function pastDate() {
  const past = moment().utc();
  past.subtract(1, 'year');
  return past.toDate();
}

function futureDate() {
  const future = moment().utc();
  future.add(1, 'year');
  return future.toDate();
}

const getPendingNotification = ([installation, notification]) =>
  new PendingNotification({
    installation,
    notification,
    sendAfter: pastDate(),
  }).save();

test('should have nothing to do when no push notifications are ready to send', () =>
  new PendingNotification({ sendAfter: futureDate() })
    .save()
    .then(() => schedule())
    .then(() => new Parse.Query('PendingNotification').find())
    .then(results => expect(results).toHaveLength(1)));

test('should send a notification and then remove it from the queue', () =>
  Promise.resolve(true)
    .then(() => Parse.Push.send.mockReset())
    .then(() =>
      Promise.all([
        new Installation({
          deviceToken: 'foo',
          timeZone: 'Europe/London',
        }).save(),
        new Notification({
          type: 'welcome',
          message: 'Explore the app!',
        }).save(),
      ])
    )
    .then(getPendingNotification)
    .then(() => schedule())
    .then(results => expect(results).toHaveLength(1))
    .then(() => new Parse.Query('PendingNotification').find())
    .then(results => expect(results).toHaveLength(0))
    .then(() => expect(Parse.Push.send).toHaveBeenCalledTimes(1)));

test('should send a notification and then requeue it for the next day', () =>
  Promise.resolve(true)
    .then(() => Parse.Push.send.mockReset())
    .then(() =>
      Promise.all([
        new Installation({
          deviceToken: 'foo',
          timeZone: 'Europe/London',
        }).save(),
        new Notification({
          type: 'welcome',
          message: 'Explore the app!',
          repeat: 'daily/10:30',
        }).save(),
      ])
    )
    .then(getPendingNotification)
    .then(() => schedule())
    .then(results => expect(results).toHaveLength(1))
    .then(() => new Parse.Query('PendingNotification').find())
    .then(results => expect(results).toHaveLength(1))
    .then(() => expect(Parse.Push.send).toHaveBeenCalledTimes(1)));

test('should work with multiple installations', () =>
  Promise.resolve(true)
    .then(() => Parse.Push.send.mockReset())
    .then(() =>
      Promise.all([
        new Installation({
          deviceToken: 'foo',
          timeZone: 'Europe/London',
        }).save(),
        new Installation({
          deviceToken: 'bar',
          timeZone: 'Europe/London',
        }).save(),
        new Notification({
          type: 'welcome',
          message: 'Explore the app!',
          repeat: 'daily/10:30',
        }).save(),
      ])
    )
    .then(([ins1, ins2, notification]) =>
      new PendingNotification({
        notification,
        timeZone: 'Europe/London',
        sendAfter: pastDate(),
      })
        .save()
        .then(pending =>
          pending
            .relation('installations')
            .add([ins1, ins2])
            .save()
        )
    )
    .then(() => schedule())
    .then(results => expect(results).toHaveLength(1))
    .then(() => new Parse.Query('PendingNotification').find())
    .then(results => expect(results).toHaveLength(1))
    .then(() => expect(Parse.Push.send).toHaveBeenCalledTimes(1)));
