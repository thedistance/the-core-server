/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-27T09:30:14+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:24:35+01:00
 * @Copyright: The Distance
 */

const moment = require('moment');
const mockdate = require('mockdate');
const getNextOccurrence = require('./getNextOccurrence');

test('should work for next day', () => {
  const repeat = 'daily/8:30';
  const timezone = 'Europe/London';
  const date = moment.utc();

  expect(
    moment(getNextOccurrence(date, repeat, timezone))
      .startOf('day')
      .diff(moment(date).startOf('day'), 'day')
  ).toEqual(1);
});

test('should work for tuesday if today is monday', () => {
  const repeat = 'tuesday/8:30';
  const timezone = 'Europe/London';
  const date = moment
    .utc()
    .day('monday')
    .toDate();

  expect(
    moment(getNextOccurrence(date, repeat, timezone))
      .startOf('day')
      .diff(moment(date).startOf('day'), 'day')
  ).toEqual(1);
});

test('should work for next monday if today is tuesday', () => {
  const repeat = 'monday/10:30';
  const timezone = 'Europe/London';
  const date = moment
    .utc()
    .day('tuesday')
    .toDate();

  expect(
    moment(getNextOccurrence(date, repeat, timezone))
      .startOf('day')
      .diff(moment(date).startOf('day'), 'days')
  ).toEqual(6);
});

test('should work for today if the repeat time is after the current time', () => {
  const repeat = 'monday/10:30';
  const timezone = 'Europe/London';
  const date = moment
    .utc({ hour: 4, minute: 30 })
    .day('monday')
    .toDate();

  expect(
    getNextOccurrence(date, repeat, timezone).isSameOrAfter(
      moment(date).add(2, 'hours')
    )
  ).toEqual(true);
});

test('should roll over to next week if the repeat time is before the current time', () => {
  const repeat = 'monday/10:30';
  const timezone = 'Europe/London';
  const date = moment
    .utc({ hour: 12, minute: 30 })
    .day('monday')
    .toDate();

  expect(
    moment(getNextOccurrence(date, repeat, timezone))
      .startOf('day')
      .diff(moment(date).startOf('day'), 'week')
  ).toEqual(1);
});

test('should set the appropriate utc date for other timezones', () => {
  mockdate.set('2018/01/05 21:00');
  const repeat = 'friday/13:00';
  const timezone = 'America/Los_Angeles';
  const date = moment.utc();

  expect(
    moment
      .utc(getNextOccurrence(date, repeat, timezone))
      .isSame(moment.utc(date).add(1, 'week'))
  ).toEqual(true);
});
