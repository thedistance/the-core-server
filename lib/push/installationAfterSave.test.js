/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-30T10:41:20+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-11-12T13:52:28+00:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const afterSave = require('./installationAfterSave');

const Notification = Parse.Object.extend('Notification');
const PendingNotification = Parse.Object.extend('PendingNotification');
const Installation = Parse.Object.extend('_Installation');

beforeEach(() => {
  ParseMockDB.mockDB();
  ParseMockDB.registerHook('_Installation', 'afterSave', afterSave);
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('it should schedule one pending notification after it is saved for the first time', () =>
  Promise.resolve(true)
    .then(() =>
      new Notification({
        message: 'Hello, world',
        type: 'hello',
        trigger: 'installation/02:00',
      }).save()
    )
    .then(() =>
      new Installation({
        timeZone: 'America/Los_Angeles',
      }).save()
    )
    .then(() => new Parse.Query(PendingNotification).find())
    .then(notifications => {
      expect(notifications).toHaveLength(1);
    }));

test('it should schedule multiple pending notifications after it is saved for the first time', () =>
  Promise.resolve([
    new Notification({
      message: 'Hello, world',
      type: 'hello',
      trigger: 'installation/04:00',
    }).save(),
    new Notification({
      message: 'Hello again, world',
      type: 'hello',
      trigger: 'installation/08:00',
    }).save(),
  ])
    .then(() =>
      new Installation({
        timeZone: 'America/Los_Angeles',
      }).save()
    )
    .then(() => new Parse.Query(PendingNotification).find())
    .then(notifications => {
      expect(notifications).toHaveLength(2);
    }));

test('it should not schedule a custom trigger automatically', () =>
  Promise.resolve(true)
    .then(() =>
      new Notification({
        message: 'Share the app with your friends and family!',
        trigger: 'after 10 sessions',
        type: 'share:1',
      }).save()
    )
    .then(() =>
      new Installation({
        timeZone: 'America/Los_Angeles',
        sessionCount: 1,
      }).save()
    )
    .then(() => new Parse.Query(PendingNotification).find())
    .then(notifications => {
      expect(notifications).toHaveLength(0);
    }));

test('should not create pending notifications a second time', () =>
  Promise.resolve(true)
    .then(() =>
      new Notification({
        message: 'Hello, world',
        type: 'hello',
        trigger: 'installation/04:00',
      }).save()
    )
    .then(() =>
      new Installation({
        timeZone: 'America/Los_Angeles',
        sessionCount: 1,
      }).save()
    )
    .then(installation => installation.save({ sessionCount: 2 }))
    .then(() => new Parse.Query(PendingNotification).find())
    .then(notifications => {
      expect(notifications).toHaveLength(1);
    }));

test('it should group notifications together with other users', () =>
  Promise.resolve(true)
    .then(() =>
      new Notification({
        trigger: 'friday/11:00',
        group: true,
      }).save()
    )
    .then(() => new Installation({ timeZone: 'America/Los_Angeles' }).save())
    .then(() =>
      new Parse.Query(PendingNotification).first().then(notification =>
        notification
          .relation('installations')
          .query()
          .count()
      )
    )
    .then(count => expect(count).toEqual(1)));

test('it should group existing notifications together with other users', () =>
  Promise.resolve(true)
    .then(() =>
      new Notification({
        trigger: 'friday/11:00',
        group: true,
      }).save()
    )
    .then(notification =>
      new PendingNotification({
        notification,
        timeZone: 'America/Los_Angeles',
      }).save()
    )
    .then(() => new Installation({ timeZone: 'America/Los_Angeles' }).save())
    .then(() =>
      new Parse.Query(PendingNotification).first().then(notification =>
        notification
          .relation('installations')
          .query()
          .count()
      )
    )
    .then(count => expect(count).toEqual(1)));
