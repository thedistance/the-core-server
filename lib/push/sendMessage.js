/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-07T09:33:59+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:25:29+01:00
 * @Copyright: The Distance
 */

const sendMessage = (where, data) =>
  Parse.Push.send(
    {
      where,
      data,
    },
    { useMasterKey: true }
  );

module.exports = sendMessage;
