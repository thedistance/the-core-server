/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-09T12:29:05+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:25:26+01:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const sendBetweenOffsets = require('./sendBetweenOffsets');
const createSuperUser = require('./../createSuperUser');

const Notification = Parse.Object.extend('Notification');
const PendingNotification = Parse.Object.extend('PendingNotification');
const Installation = Parse.Object.extend('_Installation');

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should create pending notifications to send at different times', () =>
  Promise.all([
    new Installation({
      deviceToken: 'bar',
      timeZone: 'America/Los_Angeles',
    }).save(),
    new Installation({
      deviceToken: 'bar',
      timeZone: 'Europe/London',
    }).save(),
    new Installation({
      deviceToken: 'bar',
      timeZone: 'Asia/Tokyo',
    }).save(),
  ])
    .then(createSuperUser)
    .then(user =>
      sendBetweenOffsets({
        user,
        params: {
          from: '12:00',
          until: '16:00',
          message: 'Hello, world',
          date: new Date('14:00 2018-01-01'),
        },
      })
    )
    .then(() => new Parse.Query(Notification).first())
    .then(notification => {
      expect(notification.get('type')).toEqual('adhoc');
      expect(notification.get('message')).toEqual('Hello, world');
      expect(notification.get('trigger')).toEqual('adhoc');
    })
    .then(() =>
      new Parse.Query(PendingNotification).find({ useMasterKey: true })
    )
    .then(notifications => {
      expect(notifications).toHaveLength(3);
      expect(notifications[0].get('sendAfter')).toMatchSnapshot();
      expect(notifications[1].get('sendAfter')).toMatchSnapshot();
      expect(notifications[2].get('sendAfter')).toMatchSnapshot();
    }));

test('should ensure that the from and until parameters are a valid range', () => {
  const errorHandler = jest.fn();
  return createSuperUser()
    .then(user =>
      sendBetweenOffsets(
        {
          user,
          params: {
            from: '12:00',
            until: '09:00',
            message: 'Hello, world',
            date: new Date('14:00 2018-01-01'),
          },
        },
        {
          error: errorHandler,
        }
      )
    )
    .then(() => {
      expect(errorHandler).toHaveBeenCalledTimes(1);
    });
});
