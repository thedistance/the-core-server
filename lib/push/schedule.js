/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-06T16:56:51+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-11-12T16:25:46+00:00
 * @Copyright: The Distance
 */

const moment = require('moment');
const promiseSerial = require('the-core-utils/src/promiseSerial');
const getProperty = require('the-core-utils/src/getProperty');
const processAllRecords = require('the-core-utils/src/processAllRecords');
const getNextOccurrence = require('./getNextOccurrence');
const sendMessage = require('./sendMessage');

const PendingNotification = Parse.Object.extend('PendingNotification');

const getTimezone = getProperty('timeZone');
const getUser = getProperty('user');

const sendNotification = (where, notification) =>
  sendMessage(where, {
    alert: notification.get('message'),
    type: notification.get('type'),
  });

const repeatNotification = (date, notification, timeZone) => {
  const repeat = notification.get('repeat');
  if (!repeat) {
    return false;
  }
  return getNextOccurrence(date, repeat, timeZone).toDate();
};

function createSchedule(handlers = {}) {
  function handleNotification(installation, notification, user) {
    const type = notification.get('type');
    if (Object.prototype.hasOwnProperty.call(handlers, type)) {
      return handlers[type](installation, notification, user);
    }
    return sendNotification(installation, notification);
  }

  const schedule = (date = new Date()) =>
    processAllRecords(
      promiseSerial(toSend => {
        const grouped = toSend.relation('installations').query();
        return Promise.all([
          grouped.count({ useMasterKey: true }),
          toSend.get('notification').fetch({ useMasterKey: true }),
        ])
          .then(([count, notification]) => {
            const timeZone = getTimezone(toSend);
            if (count > 0 && timeZone) {
              return Promise.resolve(
                handleNotification(grouped, notification)
              ).then(() => [timeZone, notification]);
            }
            return toSend
              .get('installation')
              .fetch({ useMasterKey: true })
              .then(installation =>
                Promise.all([
                  installation,
                  notification,
                  getUser(installation) &&
                    getUser(installation).fetch({ useMasterKey: true }),
                ])
              )
              .then(([installation, notification, user]) =>
                Promise.resolve(
                  handleNotification(installation, notification, user)
                ).then(() => [getTimezone(installation), notification])
              )
              .catch(err => {
                if (err.code === 101) {
                  console.warn(
                    `Push: Could not find installation, removing notification.`
                  );
                } else {
                  console.warn(`Push: ${err}`);
                }
                return [timeZone];
              });
          })
          .then(([timeZone, notification]) => {
            if (!notification) {
              return toSend.destroy({ useMasterKey: true });
            }
            const sendAfter = repeatNotification(date, notification, timeZone);
            if (!sendAfter) {
              return toSend.destroy({ useMasterKey: true });
            }
            return toSend.save({ sendAfter }, { useMasterKey: true });
          });
      }),
      100,
      new Parse.Query(PendingNotification).lessThanOrEqualTo(
        'sendAfter',
        moment(date)
          .utc()
          .toDate()
      ),
      { useMasterKey: true }
    ).catch(err => console.warn(err));

  return schedule;
}

module.exports = createSchedule;
