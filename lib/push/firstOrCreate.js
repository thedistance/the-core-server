/**
 * @Author: benbriggs
 * @Date:   2018-11-12T13:46:53+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-11-12T13:47:11+00:00
 * @Copyright: The Distance
 */

const R = require('ramda');

// firstOrCreate :: Parse.Object -> Object -> Parse.Object
const firstOrCreate = R.curry((ParseClass, params) =>
  Object.keys(params)
    .reduce(
      (query, key) => query.equalTo(key, params[key]),
      new Parse.Query(ParseClass)
    )
    .first({ useMasterKey: true })
    .then(record => record || new ParseClass(params).save())
);

module.exports = firstOrCreate;
