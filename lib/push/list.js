/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-19T16:55:58+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:25:00+01:00
 * @Copyright: The Distance
 */

const responseError = require('the-core-utils/src/responseError');
const responseSuccess = require('the-core-utils/src/responseSuccess');
const runWhenRoot = require('../runWhenRoot');

function list(request, response) {
  console.warn(`list is deprecated, please use listMessages instead.`);
  return runWhenRoot(request, response)
    .then(() =>
      new Parse.Query('_PushStatus')
        .limit(10)
        .descending('createdAt')
        .find({ useMasterKey: true })
    )
    .then(responseSuccess(response))
    .catch(responseError(response));
}

module.exports = list;
