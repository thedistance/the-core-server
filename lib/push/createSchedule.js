/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-07T09:47:02+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:24:18+01:00
 * @Copyright: The Distance
 */

const schedule = require('node-schedule');
const pushSchedule = require('./schedule');

// Every five minutes, by default

function createSchedule(cronTab = '*/5 * * * *') {
  function runSchedule() {
    const date = new Date();
    console.log(`Running push scheduler (${date})`);
    pushSchedule(date);
  }

  schedule.scheduleJob(cronTab, runSchedule);
}

module.exports = createSchedule;
