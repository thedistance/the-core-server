/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-16T12:09:38+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:25:53+01:00
 * @Copyright: The Distance
 */

const moment = require('moment-timezone');
const getBounds = require('./getBounds');
const sendTimeSensitivePush = require('./sendTimeSensitivePush');

const Notification = Parse.Object.extend('Notification');

function createMasterKeyNotification(params) {
  const notification = new Notification(params);
  notification.setACL(new Parse.ACL());
  return notification.save(null, { useMasterKey: true });
}
function sendBetweenOffsets({ date, message, from, until, query }) {
  const now = moment(date);
  if (getBounds(now, until).isBefore(getBounds(now, from))) {
    return Promise.reject(
      new Error(`The "from" time must be before the "until" time.`)
    );
  }
  return createMasterKeyNotification({
    message,
    type: 'adhoc',
    trigger: 'adhoc',
  }).then(notification =>
    sendTimeSensitivePush(notification, { date, from, until, query })
  );
}

module.exports = sendBetweenOffsets;
