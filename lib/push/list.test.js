/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2018-01-10T11:49:10+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:25:03+01:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const list = require('./list');
const createSuperUser = require('./../createSuperUser');

const PushStatus = Parse.Object.extend('_PushStatus');

const originalWarn = console.warn;

beforeEach(() => {
  ParseMockDB.mockDB();
  // Silence deprecation logging
  console.warn = jest.fn();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
  console.warn = originalWarn;
});

test('should list push messages', () =>
  Promise.all(
    Array.from(new Array(15), (x, index) => new PushStatus({ index }).save())
  )
    .then(() => createSuperUser())
    .then(user => list({ user }))
    .then(statuses => expect(statuses).toHaveLength(10)));
