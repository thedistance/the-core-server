/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-27T15:05:55+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-11-12T13:51:35+00:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const moment = require('moment');
const getProperty = require('the-core-utils/src/getProperty');
const getProperties = require('the-core-utils/src/getProperties');
const responseError = require('the-core-utils/src/responseError');
const responseSuccess = require('the-core-utils/src/responseSuccess');
const createPendingNotification = require('./createPendingNotification');
const firstOrCreate = require('./firstOrCreate');

const PendingNotification = Parse.Object.extend('PendingNotification');

// isFalsey :: * -> Boolean
const isFalsey = R.either(R.isNil, R.equals(false));

// isFalseyProp :: String -> Parse.Object -> Boolean
const isFalseyProp = prop => R.compose(isFalsey, getProperty(prop));

function installationAfterSave(request, response) {
  const { object: installation } = request;
  const { createdAt, updatedAt, timeZone } = getProperties(
    ['createdAt', 'updatedAt', 'timeZone'],
    installation
  );

  // Ensure that these notifications are only created once.
  if (!moment(createdAt).isSame(moment(updatedAt))) {
    return new Parse.Promise(installation)
      .then(responseSuccess(response))
      .catch(responseError(response));
  }

  return new Parse.Query('Notification')
    .matches('trigger', /\//)
    .find({ useMasterKey: true })
    .then(notifications =>
      Promise.all(
        notifications.map(notification => {
          if (isFalseyProp('group')(notification)) {
            return createPendingNotification(installation, notification);
          }

          return firstOrCreate(PendingNotification, {
            notification,
            timeZone,
          }).then(pending =>
            pending
              .relation('installations')
              .add(installation)
              .save()
          );
        })
      )
    )
    .then(R.always(installation))
    .then(responseSuccess(response))
    .catch(responseError(response));
}

module.exports = installationAfterSave;
