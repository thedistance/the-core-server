/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-27T15:30:59+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:24:28+01:00
 * @Copyright: The Distance
 */

const moment = require('moment-timezone');
const mockdate = require('mockdate');
const getFirstOccurrence = require('./getFirstOccurrence');

test('should handle minutes after installation', () => {
  const trigger = 'installation/00:30';
  const timezone = 'Europe/London';
  const date = moment.utc({ hour: 20, minute: 0 });

  expect(
    moment(getFirstOccurrence(date, trigger, timezone)).isSame(
      moment(date).add(30, 'minutes')
    )
  ).toEqual(true);
});

test('should handle hours after installation', () => {
  const trigger = 'installation/20:00';
  const timezone = 'Europe/London';
  const date = moment.utc({ hour: 20, minute: 0 });

  expect(
    moment(getFirstOccurrence(date, trigger, timezone)).isSame(
      moment(date).add(20, 'hours')
    )
  ).toEqual(true);
});

test('should handle 1 day after installation', () => {
  const trigger = 'installation/1 day';
  const timezone = 'Europe/London';
  const date = moment.utc({ hour: 20, minute: 0 });

  expect(
    moment(getFirstOccurrence(date, trigger, timezone)).isSame(
      moment(date).add(1, 'day')
    )
  ).toEqual(true);
});

test('should handle 2 days after installation', () => {
  const trigger = 'installation/2 days';
  const timezone = 'Europe/London';
  const date = moment.utc({ hour: 20, minute: 0 });

  expect(
    moment(getFirstOccurrence(date, trigger, timezone)).isSame(
      moment(date).add(2, 'days')
    )
  ).toEqual(true);
});

test('should handle 1 week after installation', () => {
  mockdate.set('2018/01/01 12:00');
  const trigger = 'installation/1 week';
  const timezone = 'Europe/London';
  const date = moment.utc({ hour: 20, minute: 0 });

  expect(
    moment(getFirstOccurrence(date, trigger, timezone)).isSame(
      moment(date).add(1, 'week')
    )
  ).toEqual(true);
});

test('should handle 2 weeks after installation', () => {
  mockdate.set('2018/01/01 12:00');
  const trigger = 'installation/2 weeks';
  const timezone = 'Europe/London';
  const date = moment.utc({ hour: 20, minute: 0 });

  expect(
    moment(getFirstOccurrence(date, trigger, timezone)).isSame(
      moment(date).add(2, 'weeks')
    )
  ).toEqual(true);
});

test('should set the appropriate utc date for other timezones', () => {
  const trigger = 'installation/5:00';
  const timezone = 'America/Los_Angeles';
  const date = moment.utc({ hour: 5, minute: 0 });

  expect(
    moment(getFirstOccurrence(date, trigger, timezone)).isSame(
      moment(date).add(5, 'hours')
    )
  ).toEqual(true);
});

test('should schedule the first occurrence of a date notification', () => {
  const trigger = 'friday/11:00';
  const timezone = 'Etc/UTC';
  const date = moment.utc({ hour: 9, minute: 0 }).day('friday');

  expect(
    moment(getFirstOccurrence(date, trigger, timezone)).isSame(
      moment(date).add(2, 'hours')
    )
  ).toEqual(true);
});
