/*
 * @Author: benbriggs
 * @Date:   2018-05-21T09:10:14+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-31T15:59:12+01:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const R = require('ramda');
const getProperty = require('the-core-utils/src/getProperty');
const loginPermissions = require('./loginPermissions');

const mockPermissions = [
  {
    name: 'Super User',
    test: R.equals,
    features: {
      login: true,
      level: 0,
    },
  },
  {
    name: 'Organisation_Admin_',
    test: R.startsWith,
    features: {
      login: true,
      level: 1,
    },
  },
];

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

const createParams = R.compose(
  R.objOf('params'),
  R.objOf('username'),
  getProperty('username')
);

test('should set login: false for missing usernames', () =>
  Promise.resolve(loginPermissions(mockPermissions)()).then(permissions =>
    expect(permissions.login).toEqual(false)
  ));

test('should set login: false for missing user accounts', () =>
  loginPermissions(mockPermissions)({
    params: { username: "Bob's mate" },
  }).then(permissions => expect(permissions.login).toEqual(false)));

test('should set login: false for regular users', () =>
  new Parse.User({ username: 'Bob' })
    .save()
    .then(createParams)
    .then(loginPermissions(mockPermissions))
    .then(permissions => expect(permissions.login).toEqual(false)));

test('should set login: false for application subscribers', () =>
  Promise.all([
    new Parse.Role('Organisation_Subscriber_1', new Parse.ACL()),
    new Parse.User({ username: 'Subscriber Bob' }),
  ])
    .then(([role, user]) => {
      role.add('users', user);
      return role.save().then(R.always(user));
    })
    .then(createParams)
    .then(loginPermissions(mockPermissions))
    .then(permissions => {
      expect(permissions.login).toEqual(false);
    }));

test('should set login: true, level: 0 for super admin', () =>
  Promise.all([
    new Parse.Role('Super User', new Parse.ACL()),
    new Parse.User({ username: 'Super Bob' }),
  ])
    .then(([role, user]) => {
      role.add('users', user);
      return role.save().then(R.always(user));
    })
    .then(createParams)
    .then(loginPermissions(mockPermissions))
    .then(permissions => {
      expect(permissions.login).toEqual(true);
      expect(permissions.level).toEqual(0);
    }));

test('should set login: true, level: 1 for organisation admin', () =>
  Promise.all([
    new Parse.Role('Organisation_Admin_1', new Parse.ACL()),
    new Parse.User({ username: 'Organisation Bob' }),
  ])
    .then(([role, user]) => {
      role.add('users', user);
      return role.save().then(R.always(user));
    })
    .then(createParams)
    .then(loginPermissions(mockPermissions))
    .then(permissions => {
      expect(permissions.login).toEqual(true);
      expect(permissions.level).toEqual(1);
    }));

test('should merge permissions into a single object', () =>
  Promise.all([
    new Parse.Role('Organisation_Admin_1', new Parse.ACL()),
    new Parse.User({ username: 'Super Bob' }),
  ])
    .then(([role, user]) => {
      role.add('users', user);
      return role.save().then(R.always(user));
    })
    .then(createParams)
    .then(
      loginPermissions([
        {
          name: 'Organisation_Admin_',
          test: R.startsWith,
          features: {
            login: true,
          },
        },
        {
          name: 'Organisation_Admin_1',
          test: R.startsWith,
          features: {
            level: 1,
          },
        },
        {
          name: 'Organisation_Admin_2',
          test: R.startsWith,
          features: {
            level: 2,
          },
        },
      ])
    )
    .then(permissions => {
      expect(permissions.login).toEqual(true);
      expect(permissions.level).toEqual(1);
    }));
