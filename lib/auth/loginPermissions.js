/*
 * @Author: benbriggs
 * @Date:   2018-05-21T09:09:47+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-06-25T10:20:40+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const responseError = require('the-core-utils/src/responseError');
const responseSuccess = require('the-core-utils/src/responseSuccess');
const getProperty = require('the-core-utils/src/getProperty');

/**
 * Defines the permissions that a role should grant a user when they log in
 * to the core. Multiple roles are supported; the function will merge the
 * permissions into a single object at the end. Any roles that are not defined
 * in the permissions configuration will always return `{ login: false }`.
 *
 * @func
 * @since v1.7.0
 * @param  {Array} permissions An array of permissions.
 * @return {Function}          A cloud function which returns an object of
 * permissions for a given user.
 * @example
 * const R = require('ramda');
 * const loginPermissions = require('the-core-server/lib/auth/loginPermissions');
 *
 * Parse.Cloud.define('1-login-permissions', loginPermissions([{
 *  name: 'Super User',  // Role name
 *  test: R.equals,      // For static roles, use equality
 *  permissions: {
 *    login: true,       // This must be defined, otherwise the user can't login
 *                       // ... Other permissions
 *  }
 * }, {
 *  name: 'Organisation_Admin_',
 *  test: R.startsWith,  // For dynamic roles, check that the prefix matches.
 *  permissions: {
 *    login: true,
 *  }
 * }]))
 */

const loginPermissions = permissions => (request, response) => {
  const success = responseSuccess(response);
  const requestUser = R.prop('user', request);
  const username = R.path(['params', 'username'], request);

  const userQuery = username
    ? new Parse.Query(Parse.User)
        .equalTo('username', username)
        .first({ useMasterKey: true })
    : requestUser ? Promise.resolve(requestUser) : false;

  if (!userQuery) {
    return success({ login: false });
  }

  return userQuery
    .then(user => {
      if (!user) {
        return { login: false };
      }
      return new Parse.Query(Parse.Role)
        .equalTo('users', user)
        .find({ useMasterKey: true })
        .then(
          R.compose(
            R.when(R.isEmpty, R.always({ login: false })),
            R.mergeAll,
            R.map(
              R.compose(
                R.reduce(R.mergeDeepRight, {}),
                R.map(R.prop('features')),
                role =>
                  R.filter(
                    permission => permission.test(permission.name, role),
                    permissions
                  ),
                getProperty('name')
              )
            )
          )
        );
    })
    .then(success)
    .catch(responseError(response));
};

module.exports = loginPermissions;
