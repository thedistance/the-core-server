/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-11-07T12:21:50+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:23:12+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const responseError = require('the-core-utils/src/responseError');
const responseSuccess = require('the-core-utils/src/responseSuccess');

function canLogin(request, response) {
  const { username } = request.params;

  return new Parse.Query(Parse.User)
    .equalTo('username', username)
    .first()
    .then(user => {
      if (!user) {
        return responseSuccess(response, { isAdmin: false });
      }
      return new Parse.Query(Parse.Role)
        .equalTo('code', 'root')
        .equalTo('users', user)
        .first()
        .then(Boolean)
        .then(R.objOf('isAdmin'))
        .then(responseSuccess(response));
    })
    .catch(responseError(response));
}

module.exports = canLogin;
