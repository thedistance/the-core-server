/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-11-21T15:51:10+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:23:27+01:00
 * @Copyright: The Distance
 */

const {
  EMAIL_FROM,
  EMAIL_ACCESS_KEY_ID,
  EMAIL_SECRET_ACCESS_KEY,
  EMAIL_REGION,
} = process.env;

let adapter = null; // Override via one of the below methods;

if (process.env.NODE_ENV === 'development') {
  const smtpAdapter = require('simple-parse-smtp-adapter');

  adapter = smtpAdapter({
    host: 'localhost',
    port: 1025,
    user: 'mail',
    password: 'pass',
    fromAddress: EMAIL_FROM,
  });
} else if (
  EMAIL_FROM &&
  EMAIL_ACCESS_KEY_ID &&
  EMAIL_SECRET_ACCESS_KEY &&
  EMAIL_REGION
) {
  const sesAdapter = require('parse-server-amazon-ses-adapter');

  adapter = sesAdapter({
    from: EMAIL_FROM,
    accessKeyId: EMAIL_ACCESS_KEY_ID,
    secretAccessKey: EMAIL_SECRET_ACCESS_KEY,
    region: EMAIL_REGION,
    verificationSubject: 'Please verify your e-mail for %appname%',
    verificationBody:
      'Hi,\n\nYou are being asked to confirm the e-mail address %email% with %appname%\n\nClick here to confirm it:\n%link%',
    passwordResetSubject: 'Password Reset Request for %appname%',
    passwordResetBody:
      'Hi,\n\nYou requested a password reset for %appname%.\n\nClick here to reset it:\n%link%',
  });
}

module.exports = adapter;
