/*
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-12-15T10:24:50+00:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-21T09:23:31+01:00
 * @Copyright: The Distance
 */

/* eslint-disable camelcase */

// Helper functions for accessing the linkedin API.
const got = require('got');
const Parse = require('parse/node').Parse;

// Returns a promise that fulfills iff this user id is valid.
function validateAuthData(authData) {
  return request('people/~:(id)', authData.access_token, authData.is_mobile_sdk)
    .then(data => {
      if (data && data.id === authData.id) {
        return;
      }
      throw new Parse.Error(
        Parse.Error.OBJECT_NOT_FOUND,
        'Linkedin auth is invalid for this user.'
      );
    })
    .catch(() => {
      throw new Parse.Error(
        Parse.Error.OBJECT_NOT_FOUND,
        'Failed to get a response from LinkedIn.'
      );
    });
}

// Returns a promise that fulfills iff this app id is valid.
function validateAppId() {
  return Promise.resolve();
}

// A promisey wrapper for api requests
function request(path, access_token, is_mobile_sdk) {
  const headers = {
    'x-li-format': 'json',
  };

  if (is_mobile_sdk) {
    headers['x-li-src'] = 'msdk';
  }

  return got
    .get(`https://api.linkedin.com/v1/${path}`, {
      headers,
      query: {
        oauth2_access_token: access_token,
      },
      json: true,
    })
    .then(({ body }) => body)
    .catch(err => {
      if (err.response.statusCode === 401) {
        return got
          .get(`https://api.linkedin.com/v1/${path}`, {
            headers: Object.assign({}, headers, {
              Authorization: `Bearer ${access_token}`,
            }),
            json: true,
          })
          .then(({ body }) => body)
          .catch(() => {
            throw new Parse.Error(
              Parse.Error.OBJECT_NOT_FOUND,
              'Failed to get a response from LinkedIn.'
            );
          });
      }
      throw new Parse.Error(
        Parse.Error.OBJECT_NOT_FOUND,
        'Failed to get a response from LinkedIn.'
      );
    });
}

module.exports = {
  validateAppId,
  validateAuthData,
};
