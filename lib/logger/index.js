/**
 * @Author: benbriggs
 * @Date:   2018-09-24T08:57:34+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-09-24T08:58:43+01:00
 * @Copyright: The Distance
 */

const winston = require('winston');

// No need to log to a file as passenger routes our logs
// through to nginx' logging handler. If we did file logging,
// we would get the same message twice in different files!

const logger = new winston.Logger({
  level: 'info',
  transports: [
    new winston.transports.Console({
      colorize: true,
    }),
  ],
});

class WinstonLogger {
  log() {
    return logger.log.apply(logger, arguments);
  }
}

module.exports = WinstonLogger;
