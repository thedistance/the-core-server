/*
 * @Author: benbriggs
 * @Date:   2018-05-21T16:22:45+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T12:43:34+01:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const R = require('ramda');
const nock = require('nock');
const getProperty = require('the-core-utils/src/getProperty');
const findOne = require('./findOne');

const Bar = Parse.Object.extend('Bar');

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should find a single record', () => {
  nock(process.env.SERVER_URL)
    .get('/schemas/Bar')
    .reply(200, {
      classLevelPermissions: {
        find: {
          '*': true,
        },
        get: {
          '*': true,
        },
        create: {
          '*': true,
        },
        update: {
          '*': true,
        },
        delete: {
          '*': true,
        },
        addField: {},
      },
    });

  return new Bar()
    .save()
    .then(R.compose(R.objOf('params'), R.objOf('id'), getProperty('id')))
    .then(findOne({ className: 'Bar' }))
    .then(result => expect(result.object).toBeInstanceOf(Bar));
});

test('should show write access', () => {
  nock(process.env.SERVER_URL)
    .get('/schemas/Bar')
    .reply(200, {
      classLevelPermissions: {
        find: {
          '*': true,
        },
        get: {
          '*': true,
        },
        create: {
          '*': true,
        },
        update: {
          '*': true,
        },
        delete: {
          '*': true,
        },
        addField: {},
      },
    });

  return new Bar()
    .save()
    .then(R.compose(R.objOf('params'), R.objOf('id'), getProperty('id')))
    .then(findOne({ className: 'Bar' }))
    .then(result =>
      expect(result.permissions).toEqual({
        create: true,
        delete: true,
        update: true,
      })
    );
});
