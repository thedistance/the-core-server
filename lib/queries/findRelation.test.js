/*
 * @Author: benbriggs
 * @Date:   2018-05-23T13:17:08+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T13:53:20+01:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const nock = require('nock');
const getProperty = require('the-core-utils/src/getProperty');
const findRelation = require('./findRelation');

const Bar = Parse.Object.extend('Bar');
const Foo = Parse.Object.extend('Foo');

beforeEach(() => {
  process.env.SERVER_URL = 'http://localhost/parse';
  process.env.MASTER_KEY = 'parse';
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should find records', () => {
  nock(process.env.SERVER_URL)
    .get('/schemas/Foo')
    .reply(200, {});

  return Promise.all([
    new Bar().save(),
    Promise.all(Array.from(Array(10), () => new Foo().save())),
  ])
    .then(([bar, foos]) => {
      bar.relation('foo').add(foos);
      return bar.save();
    })
    .then(getProperty('id'))
    .then(id =>
      findRelation({
        className: 'Bar',
        targetClassName: 'Foo',
        targetColumn: 'foo',
      })({ params: { id } })
    )
    .then(result => expect(result.data).toHaveLength(10));
});

test('should return an object of permissions when public access is set', () => {
  nock(process.env.SERVER_URL)
    .get('/schemas/Foo')
    .reply(200, {
      classLevelPermissions: {
        find: {
          '*': true,
        },
        get: {
          '*': true,
        },
        create: {
          '*': true,
        },
        update: {
          '*': true,
        },
        delete: {
          '*': true,
        },
        addField: {},
      },
    });

  return Promise.all([
    new Bar().save(),
    Promise.all(Array.from(Array(10), () => new Foo().save())),
  ])
    .then(([bar, foos]) => {
      bar.relation('foo').add(foos);
      return bar.save();
    })
    .then(getProperty('id'))
    .then(id =>
      findRelation({
        className: 'Bar',
        targetClassName: 'Foo',
        targetColumn: 'foo',
      })({ params: { id } })
    )
    .then(result =>
      expect(result.permissions).toEqual({
        create: true,
        delete: true,
        update: true,
      })
    );
});
