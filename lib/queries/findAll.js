/*
 * @Author: benbriggs
 * @Date:   2018-05-21T14:05:47+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-24T12:35:26+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const getSessionToken = require('the-core-utils/src/getSessionToken');
const responseError = require('the-core-utils/src/responseError');
const escapeStringRegexp = require('escape-string-regexp');
const formatQueryResults = require('../util/formatQueryResults');
const findPermissions = require('./findPermissions');
const errorTransformer = require('./errorTransformer');

const findAll = ({ className, queryConstraints = R.identity }) => (
  request,
  response
) => {
  const { pageSize = 20, page = 0, sorted, filtered } = request.params || {};

  const token = getSessionToken(request.user);
  const query = queryConstraints(new Parse.Query(className));

  if (filtered && filtered.length) {
    filtered.forEach(({ id, value }) =>
      query.matches(id, new RegExp(escapeStringRegexp(value), 'i'))
    );
  }

  if (sorted && sorted.length) {
    sorted.forEach(
      ({ id, desc }) => (desc ? query.descending(id) : query.ascending(id))
    );
  }

  return Promise.all([
    query.count(token),
    query
      .limit(pageSize)
      .skip(pageSize * page)
      .find(token),
    new Parse.Query(Parse.Role)
      .equalTo('users', request.user)
      .find({ useMasterKey: true }),
    findPermissions(className),
  ])
    .then(formatQueryResults(pageSize, request, response))
    .catch(R.compose(responseError(response), errorTransformer));
};

module.exports = findAll;
