/*
 * @Author: benbriggs
 * @Date:   2018-05-21T16:08:19+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-24T12:36:03+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const getSessionToken = require('the-core-utils/src/getSessionToken');
const responseError = require('the-core-utils/src/responseError');
const responseSuccess = require('the-core-utils/src/responseSuccess');
const mergeDataWithRoles = require('../util/mergeDataWithRoles');
const findPermissions = require('./findPermissions');
const errorTransformer = require('./errorTransformer');

const findOne = ({ className, queryConstraints = R.identity }) => (
  request,
  response
) => {
  const error = responseError(response);
  const id = R.path(['params', 'id'], request);

  if (!id) {
    return error('Missing "id" parameter.');
  }

  const token = getSessionToken(request.user);

  return Promise.all([
    new Parse.Query(Parse.Role)
      .equalTo('users', request.user)
      .find({ useMasterKey: true }),
    queryConstraints(new Parse.Query(className)).get(id, token),
    findPermissions(className),
  ])
    .then(([roles, object, permissions]) =>
      mergeDataWithRoles(permissions, request.user, roles, object)
    )
    .then(responseSuccess(response))
    .catch(R.compose(error, errorTransformer));
};

module.exports = findOne;
