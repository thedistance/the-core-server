/**
 * @Author: benbriggs
 * @Date:   2018-05-24T12:28:11+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-24T12:34:44+01:00
 * @Copyright: The Distance
 */

const errorTransformer = err => {
  if (err.code === 119) {
    return 'Unauthorised.';
  }
  return err;
};

module.exports = errorTransformer;
