/*
 * @Author: benbriggs
 * @Date:   2018-05-23T12:37:41+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-23T13:53:33+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const got = require('got');

const findPermissions = className =>
  got
    .get(`${process.env.SERVER_URL}/schemas/${className}`, {
      headers: {
        'X-Parse-Application-Id': process.env.APP_ID,
        'X-Parse-Master-Key': process.env.MASTER_KEY,
      },
      json: true,
    })
    .then(
      R.compose(R.defaultTo({}), R.path(['body', 'classLevelPermissions']))
    );

module.exports = findPermissions;
