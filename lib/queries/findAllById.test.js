/**
 * @Author: benbriggs
 * @Date:   2018-05-31T09:09:25+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-31T09:15:29+01:00
 * @Copyright: The Distance
 */

global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const R = require('ramda');
const nock = require('nock');
const isDefined = require('the-core-utils/src/isDefined');
const getProperty = require('the-core-utils/src/getProperty');
const findAllById = require('./findAllById');

const Bar = Parse.Object.extend('Bar');
const Foo = Parse.Object.extend('Foo');

beforeEach(() => {
  process.env.SERVER_URL = 'http://localhost/parse';
  process.env.MASTER_KEY = 'parse';
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should find records for a given id', () => {
  nock(process.env.SERVER_URL)
    .get('/schemas/Bar')
    .reply(200, {});

  return new Foo()
    .save()
    .then(foo =>
      Promise.all([
        Promise.all(Array.from(Array(10), () => new Bar().save())),
        Promise.all(Array.from(Array(10), () => new Bar({ foo }).save())),
      ]).then(R.always(foo))
    )
    .then(getProperty('id'))
    .then(id => findAllById({ className: 'Bar', column: 'foo' })({ id }))
    .then(result => expect(result.data).toHaveLength(10));
});

test('should return an object of permissions when public access is set', () => {
  nock(process.env.SERVER_URL)
    .get('/schemas/Bar')
    .reply(200, {
      classLevelPermissions: {
        find: {
          '*': true,
        },
        get: {
          '*': true,
        },
        create: {
          '*': true,
        },
        update: {
          '*': true,
        },
        delete: {
          '*': true,
        },
        addField: {},
      },
    });

  return Promise.all(Array.from(Array(10), () => new Bar().save()))
    .then(() => findAllById({ className: 'Bar' })({}))
    .then(result =>
      expect(result.permissions).toEqual({
        create: true,
        delete: true,
        update: true,
      })
    );
});

test('should return an object of permissions when they have appropriate access', () => {
  nock(process.env.SERVER_URL)
    .get('/schemas/Bar')
    .reply(200, {
      classLevelPermissions: {
        find: {
          'role:Super User': true,
        },
        get: {
          'role:Super User': true,
        },
        create: {
          'role:Super User': true,
        },
        update: {
          'role:Super User': true,
        },
        delete: {
          'role:Super User': true,
        },
        addField: {},
      },
    });

  return Promise.all([
    new Parse.Role('Super User', new Parse.ACL()),
    new Parse.User(),
    Promise.all(Array.from(Array(10), () => new Bar().save())),
  ])
    .then(([role, user]) => {
      role.add('users', user);
      return role.save().then(R.always(user));
    })
    .then(user => findAllById({ className: 'Bar' })({ user }))
    .then(result =>
      expect(result.permissions).toEqual({
        create: true,
        delete: true,
        update: true,
      })
    );
});

test('should calculate page size', () => {
  nock(process.env.SERVER_URL)
    .get('/schemas/Bar')
    .reply(200, {});

  return Promise.all(Array.from(Array(10), () => new Bar().save()))
    .then(() => findAllById({ className: 'Bar' })({ params: { pageSize: 5 } }))
    .then(result => expect(result.pages).toEqual(2));
});

test('should show write access', () => {
  nock(process.env.SERVER_URL)
    .get('/schemas/Bar')
    .reply(200, {});

  return Promise.all(Array.from(Array(10), () => new Bar().save()))
    .then(() => findAllById({ className: 'Bar' })({}))
    .then(result =>
      expect(
        result.data.every(R.compose(isDefined, R.prop('permissions')))
      ).toEqual(true)
    );
});
