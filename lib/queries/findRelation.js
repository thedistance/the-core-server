/*
 * @Author: benbriggs
 * @Date:   2018-05-23T13:11:04+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-05-24T12:36:35+01:00
 * @Copyright: The Distance
 */

const R = require('ramda');
const getSessionToken = require('the-core-utils/src/getSessionToken');
const responseError = require('the-core-utils/src/responseError');
const escapeStringRegexp = require('escape-string-regexp');
const formatQueryResults = require('../util/formatQueryResults');
const findPermissions = require('./findPermissions');
const errorTransformer = require('./errorTransformer');

const findRelation = ({
  className,
  targetClassName,
  targetColumn,
  queryConstraints = R.identity,
}) => (request, response) => {
  const { pageSize = 20, page = 0, sorted, filtered, id } =
    request.params || {};

  const token = getSessionToken(request.user);

  return Promise.all([
    new Parse.Query(className)
      .get(id, token)
      .then(target => {
        const query = queryConstraints(target.relation(targetColumn).query());

        if (filtered && filtered.length) {
          filtered.forEach(({ id, value }) =>
            query.matches(id, new RegExp(escapeStringRegexp(value), 'i'))
          );
        }

        if (sorted && sorted.length) {
          sorted.forEach(
            ({ id, desc }) =>
              desc ? query.descending(id) : query.ascending(id)
          );
        }

        return query;
      })
      .then(query =>
        Promise.all([
          query.count(token),
          query
            .limit(pageSize)
            .skip(pageSize * page)
            .find(token),
        ])
      ),
    new Parse.Query(Parse.Role)
      .equalTo('users', request.user)
      .find({ useMasterKey: true }),
    findPermissions(targetClassName),
  ])
    .then(([[count, data], roles, permissions]) => [
      count,
      data,
      roles,
      permissions,
    ])
    .then(formatQueryResults(pageSize, request, response))
    .catch(R.compose(responseError(response), errorTransformer));
};

module.exports = findRelation;
